// ============================================================================
//
// = CONTEXT
//		TANGO Project - Steerer control
//
// = File
//		SteererTask.cpp
//
// = AUTHOR
//		jean coquet d'apres le code de Nicolas Leclercq - SOLEIL
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <math.h>
#include <yat/threading/Mutex.h>
#include <yat/time/Timer.h>

#include "SteererTask.h"

// ============================================================================
// SOME CONSTs
// ============================================================================
// ----------------------------------------------------------------------------
static const double __NAN__ = ::sqrt(-1);
// ----------------------------------------------------------------------------
 

namespace Steerer_ns
{

// ============================================================================
// steerer_cfg::steerer_cfg
// ============================================================================
steerer_cfg::steerer_cfg () : 
   ip_addr (std::string("192.168.0.0")),
   axis_letter (std::string("X")),
   axis_number (0),
   steerer_nb (0)
{
  //- noop ctor
}
steerer_cfg::steerer_cfg (std::string _ip_addr, std::string _axis_letter, short _steerer_nb) : 
   ip_addr (_ip_addr),
   axis_letter (_axis_letter),
   steerer_nb (_steerer_nb)
{
  //- compute axis number
	axis_number = axis_letter.find ("Z") != std::string::npos ? 1 : 0;

}
steerer_cfg::steerer_cfg (const steerer_cfg & _src)
{
  *this = _src;
}

// ============================================================================
// steerer_cfg::operator =
// ============================================================================
void steerer_cfg::operator = (const steerer_cfg & _src)
{
  ip_addr     = _src.ip_addr;
  axis_letter = _src.axis_letter;
  steerer_nb  = _src.steerer_nb;
  axis_number = _src.axis_number;
}


// ============================================================================
// data::data
// ============================================================================
data::data ()
: 
  is_fofb_controlled (false),
  is_on (false),
  current (__NAN__),
  voltage (__NAN__),
  setpoint (__NAN__),
  nb_frames_sec (0),
  nb_errors_sec (0),
  sp_min (__NAN__),
  sp_mean (__NAN__),
  sp_max (__NAN__),
  error_counter (0)
{
  //- noop ctor
}
data::data (const data & _src)
{
  *this = _src;
}
// ============================================================================
// data::operator =
// ============================================================================
void data::operator = (const data & _src)
{
  is_fofb_controlled = _src.is_fofb_controlled;
  is_on = _src.is_on;
  current  = _src.current;
  voltage  = _src.voltage;
  setpoint  = _src.setpoint;
  nb_frames_sec  = _src.nb_frames_sec;
  nb_errors_sec  = _src.nb_errors_sec;
  sp_min  = _src.sp_min;
  sp_mean  = _src.sp_mean;
  sp_max  = _src.sp_max;
  error_counter  = _src.error_counter;
  status  = _src.status;
  state  = _src.state;
}



	// ======================================================================
	// SteererTask::SteererTask
	// ======================================================================
  SteererTask::SteererTask (size_t _periodic_message_period_ms, 
                              Tango::DeviceImpl * _host_device ,  
                              steerer_cfg & _cfg)
    : yat4tango::DeviceTask(_host_device),
      steerer_config (_cfg)
	{
		//- reset data content to NAN or 0
    this->m_data.meascurr = __NAN__;
    this->m_data.measvolt = __NAN__;
    this->f_data.min_sett_sec = __NAN__;
    this->f_data.max_sett_sec = __NAN__;
    this->f_data.average_sett_sec = __NAN__;
    this->f_data.nbrframe_sec = 0;
    this->f_data.nbrferr_sec = 0;
    this->f_data.nbrferr = 0;
    this->communication_error = false;
    this->com_state = COM_NOT_INIT;
    this->get_index = GET_M_DATA;
    this->communication_error_counter = 0;
    this->m_steerer = 0;
    this->last_error = "no error";

    //- configure optional msg handling
    this->enable_timeout_msg(false);
    this->enable_periodic_msg(true);
    this->set_periodic_msg_period(_periodic_message_period_ms);
	}

	// ======================================================================
	// SteererTask::~SteererTask
	// ======================================================================
	SteererTask::~SteererTask (void)
	{
    //- noop
	}

	// ============================================================================
	// SteererTask::process_message
	// ============================================================================
	void SteererTask::process_message (yat::Message& _msg) throw (Tango::DevFailed)
	{
	  DEBUG_STREAM << "SteererTask::handle_message::receiving msg " << _msg.to_string() << std::endl;

	  //- handle msg
    switch (_msg.type())
	  {
	    //- THREAD_INIT ----------------------
	    case yat::TASK_INIT:
	      {
  	      DEBUG_STREAM << "SteererTask::handle_message::THREAD_INIT::thread is starting up" << std::endl;

  	      //- "initialization" code goes here
          //----------------------------------------------------

  	      //- instanciate our ThreadSafeDeviceProxyHelper
          this->reconnect_to_HW ();
        } 
		    break;
        
		  //- THREAD_EXIT ----------------------
		  case yat::TASK_EXIT:
		    {
  			  DEBUG_STREAM << "SteererTask::handle_message::THREAD_EXIT::thread is quitting" << std::endl;

  			  //- "release" code goes here
          //----------------------------------------------------
          
  			  //- release the ThreadSafeDeviceProxyHelper
          if (this->m_steerer)
          {
            delete this->m_steerer;
            this->m_steerer = 0;
          }
        }
			  break;
        
		  //- THREAD_PERIODIC ------------------
		  case yat::TASK_PERIODIC:
		    {
  		    DEBUG_STREAM << "SteererTask::handle_message::handling THREAD_PERIODIC msg" << std::endl;
  		    //INFO_STREAM << "TASK_PERIODIC period : " << dt.elapsed_msec() << " ms" << std::endl;
  		    dt.restart ();

          //- code relative to the task's periodic job goes here
          //----------------------------------------------------
          //- get 1 HW data at a time
          if (get_index >= GET_MAX_INDEX)
            get_index = GET_M_DATA;


  		      //- let's get HW data
            try
            {
              switch (get_index)
              {
              case GET_M_DATA :
              {
                yat::Timer t;
                this->m_steerer->axis(this->steerer_config.axis_number).get_mdata(&this->m_data);
                DEBUG_STREAM << "Steerer::get_mdata took " << t.elapsed_msec() << " ms" << std::endl;
                { 
                  yat::AutoMutex<> guard(this->m_lock);
                  internal_data.current = this->m_data.meascurr;
                  internal_data.voltage = this->m_data.measvolt;
                  internal_data.status  = this->m_data.fail;
                  internal_data.state   = this->m_data.state;
                }
              }
              break;
              case GET_F_DATA_1 :
              case GET_F_DATA_2 :
              case GET_F_DATA_3 :
              case GET_F_DATA_4 :
              {
             
                yat::Timer t;
                this->m_steerer->axis(this->steerer_config.axis_number).get_fdata(&this->f_data);		
                DEBUG_STREAM << "Steerer::get_fdata took " << t.elapsed_msec() << " ms"  << std::endl;
                { 
                  yat::AutoMutex<> guard(this->m_lock);
                  internal_data.nb_frames_sec = this->f_data.nbrframe_sec;
                  internal_data.nb_errors_sec = this->f_data.nbrferr_sec;
                  internal_data.sp_min        = this->f_data.min_sett_sec;
                  internal_data.sp_mean       = this->f_data.average_sett_sec;
                  internal_data.sp_max        = this->f_data.max_sett_sec;
                  internal_data.error_counter = this->f_data.nbrferr;
                }
              }
              break;
              case GET_SETPOINT :
              {
                yat::Timer t;
		            double setpoint = this->m_steerer->axis(this->steerer_config.axis_number).get_current();
                DEBUG_STREAM << "Steerer::get_current took " << t.elapsed_msec() << " ms"  << std::endl;
		            { 
                  yat::AutoMutex<> guard(this->m_lock);
                  internal_data.setpoint = setpoint;
                }
              }
              break;
              case GET_SWITCH :
              {
                yat::Timer t;
                int switch_state = this->m_steerer->axis(this->steerer_config.axis_number).get_switch();
                DEBUG_STREAM << "Steerer::get_switch took " << t.elapsed_msec() << " ms"  << std::endl;
		            { 
                  yat::AutoMutex<> guard(this->m_lock);
                  internal_data.is_fofb_controlled = switch_state != 0 ? true : false;
                }
              }
              break;
              case GET_POWER_STATE :
              {
                yat::Timer t;
 		            int power_state = this->m_steerer->axis(this->steerer_config.axis_number).get_state();
                DEBUG_STREAM << "Steerer::get_state took " << t.elapsed_msec() << " ms"  << std::endl;
		            { 
                  yat::AutoMutex<> guard(this->m_lock);
                  internal_data.is_on = power_state != 0 ? true : false;
                }
              }
              break;
              default : 
                this->get_index = GET_M_DATA;
              break;
              } //- switch
              this->com_status = "communication OK";
              //- increment the read index
              this->get_index++;

            }
	          catch(itest::ItestException &e)
	          {
              this->com_state = COM_ERROR;
              this->last_error = "error trying to read data on the HW" + e.errors[0].desc;
              ERROR_STREAM << "communication error [exception caught]" << std::endl;
		          for(size_t i=0; i< e.errors.size(); i++)
  		          ERROR_STREAM << e.errors[i].reason << " :: "
			                       << e.errors[i].desc   << " :: "
									           << e.errors[i].origin << " :: "
									           << " code : " << e.errors[i].code 
									           << std::endl;
              this->com_status = "device initialization failed ["
                                 + e.errors[0].desc
                                 + std::string("]");
		          this->reconnect_to_HW();
              return;
            }
            catch (...)
            {
              this->com_state = COM_ERROR;
              this->com_status = "communication failed trying to write read data on HW";
              this->last_error = "unknown exception trying to read data on the HW" ;
              ERROR_STREAM << "Unknown exception caught while trying to read on Steerer" << std::endl;
              //- do something to manage the error...
            }
        }
		    break;
        
		  //- THREAD_TIMEOUT -------------------
		  case yat::TASK_TIMEOUT:
		    {
  		    //- code relative to the task's tmo handling goes here

  		    DEBUG_STREAM << "SteererTask::handle_message::handling THREAD_TIMEOUT msg" << std::endl;
        }
		    break;
        
		  //- USER_DEFINED_MSG -------------------------------------------------------
      //- WRITE SETPOINT ---------------------------------------------------------
		  case kWRITE_CURRENT_MSG :
		    {
  		  	DEBUG_STREAM << "SteererTask::handle_message::handling kWRITE_CURRENT_MSG user msg" << std::endl;

  		  	//- get msg data...
  		  	//- msg data is always dettached by reference (i.e. pointer) and ownership is transfered 
          //- to the caller. it means that we will have to delete the returned pointer in order to 
          //- avoid memory leaks. that's the semantic of the yat::Message::dettach_data member!
          //- thanks to the template impl of yat::Message::dettach_data, we could extract any kind
          //- of data. here we just expect a single double...
  		    double sp = _msg.get_data<double>();
          //- dump the received value
          DEBUG_STREAM << "SteererTask::handle_message::double scalar value is " 
                       << sp 
                       << std::endl;
          //- now try to write on the HW
	        try
	        {
		        this->m_steerer->axis(steerer_config.axis_number).set_current(sp);
	        }
	        catch(itest::ItestException &e)
	        {
            this->com_state = COM_ERROR;
            ERROR_STREAM << "communication error (WRITE_CURRENT_MSG) [ItestException caught]" << std::endl;
		        for(size_t i=0; i< e.errors.size(); i++)
  		        ERROR_STREAM << e.errors[i].reason << " :: "
			                     << e.errors[i].desc  << " :: "
									         << e.errors[i].origin<< " :: "
									         << " code : " << e.errors[i].code 
									         << std::endl;
            this->com_status = "communication failed trying to write current";
            this->last_error = "Itest exception"
                             + std::string( e.errors[0].desc )
                             + std::string("]");
	        }
          catch (...)
          {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error [unknown exception caught]" << std::endl;
            this->com_status = "communication error trying to write current" ;
            this->last_error = "Unknown exception";
          }
  		  }
  		  break;
      //- LOCAL/REMOTE ---------------------------------------------------------
		  case kLOCAL_CONTROL_MSG :
      case kLIBERA_CONTROL_MSG :
        {
  		  	DEBUG_STREAM << "SteererTask::handle_message::handling kLOCAL_CONTROL_MSG or kLIBERA_CONTROL_MSG user msg" << std::endl;

          //- now try to write on the HW
          bool b = _msg.type() == kLOCAL_CONTROL_MSG ? false : true;
	        try
	        {
            this->m_steerer->axis(this->steerer_config.axis_number).set_switch(b);
	        }
	        catch(itest::ItestException &e)
	        {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error LOCAL_CONTROL_MSG or LIBERA_CONTROL_MSG [ItestException caught]" << std::endl;
		        for(size_t i=0; i< e.errors.size(); i++)
  		        ERROR_STREAM << e.errors[i].reason << " :: "
			                     << e.errors[i].desc  << " :: "
									         << e.errors[i].origin<< " :: "
									         << " code : " << e.errors[i].code 
									         << std::endl;
            this->com_status = "communication error [ItestException exception caught]";
            this->last_error = "Itest exception  trying to set LOCAL CONTROL MODE or LIBERA CONTROL MODE"
                             + std::string( e.errors[0].desc )
                             + std::string("]");
	        }
          catch (...)
          {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error [unknown exception caught]" << std::endl;
            this->com_status = "communication error";
            this->last_error = "Unknown exception trying to set LOCAL CONTROL MODE or LIBERA CONTROL MODE";
          }
  		  }
  		  break;

      //- ON/OFF ---------------------------------------------------------
		  case kON_MSG :
      case kOFF_MSG :
        {
  		  	DEBUG_STREAM << "SteererTask::handle_message::handling kON_MSG or kOFF_MSG user msg" << std::endl;

          //- now try to write on the HW
          bool b = _msg.type() == kOFF_MSG ? false : true;
	        try
	        {
            //- do not power_off a already powered off ps
            if (!b)
            {
              if (this->m_steerer->axis(this->steerer_config.axis_number).get_state() == 0)
              {
                INFO_STREAM << "SteererTask::process_message PS already OFF (nothing to do) " << std::endl;
                return;
              }
            }
            this->m_steerer->axis(this->steerer_config.axis_number).set_state(b);
	        }
	        catch(itest::ItestException &e)
	        {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error kON_MSG or kOFF_MSG [ItestException caught]" << std::endl;
		        for(size_t i=0; i< e.errors.size(); i++)
  		        ERROR_STREAM << e.errors[i].reason << " :: "
			                     << e.errors[i].desc  << " :: "
									         << e.errors[i].origin<< " :: "
									         << " code : " << e.errors[i].code 
									         << std::endl;
            this->com_status = "communication failed trying to set ON or OFF";
            this->last_error = std::string( e.errors[0].desc )
                             + std::string("]");
	        }
          catch (...)
          {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error [unknown exception caught]" << std::endl;
            this->com_status = "communication error trying to set ON or OFF";
            this->last_error = "Unknown exception";
          }
  		  }
  		  break;
      //- CLEAR ERROR COUNTER ---------------------------------------------------------
		  case kCLEAR_ERROR_CNT_MSG :
        {
  		  	DEBUG_STREAM << "SteererTask::handle_message::handling kCLEAR_ERROR_CNT_MSG user msg" << std::endl;

          //- now try to write on the HW
	        try
	        {
            this->m_steerer->axis(this->steerer_config.axis_number).clear_ferrors();
            this->last_error = "No error";
	        }
	        catch(itest::ItestException &e)
	        {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error kCLEAR_ERROR_CNT_MSG [ItestException caught]" << std::endl;
		        for(size_t i=0; i< e.errors.size(); i++)
  		        ERROR_STREAM << e.errors[i].reason << " :: "
			                     << e.errors[i].desc  << " :: "
									         << e.errors[i].origin<< " :: "
									         << " code : " << e.errors[i].code 
									         << std::endl;
            this->com_status = "communication failed trying to clear error counter";
            this->last_error = "Itest exception"
                             + std::string( e.errors[0].desc )
                             + std::string("]");
	        }
          catch (...)
          {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error [unknown exception caught]" << std::endl;
            this->com_status = "communication error trying to clear error counter";
            this->last_error = "Unknown exception";
          }
  		  }
  		  break;
      //- CLEAR ALARMS ---------------------------------------------------------
		  case kCLEAR_ALARM_MSG :
        {
  		  	DEBUG_STREAM << "SteererTask::handle_message::handling kCLEAR_ALARM_MSG user msg" << std::endl;

          //- now try to write on the HW
	        try
	        {
            this->m_steerer->axis(this->steerer_config.axis_number).clear_alarm();
	        }
	        catch(itest::ItestException &e)
	        {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error kCLEAR_ALARM_MSG [ItestException caught]" << std::endl;
		        for(size_t i=0; i< e.errors.size(); i++)
  		        ERROR_STREAM << e.errors[i].reason << " :: "
			                     << e.errors[i].desc  << " :: "
									         << e.errors[i].origin<< " :: "
									         << " code : " << e.errors[i].code 
									         << std::endl;
            this->com_status = "communication failed trying to clear alarm";
            this->last_error = "Itest exception"
                             + std::string( e.errors[0].desc )
                             + std::string("]");
	        }
          catch (...)
          {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error [unknown exception caught]" << std::endl;
            this->com_status = "communication error trying to clear alarm";
            this->last_error = "Unknown exception";
          }
  		  }
  		  break;
      //- RESET ---------------------------------------------------------
		  case kRESET_MSG :
        {
  		  	DEBUG_STREAM << "SteererTask::handle_message::handling kRESET_MSG user msg" << std::endl;

          //- now try to write on the HW
	        try
	        {
            this->m_steerer->axis(this->steerer_config.axis_number).clear_alarm();
            this->last_error = "No error";
	        }
	        catch(itest::ItestException &e)
	        {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error kRESET_MSG [ItestException caught]";
		        for(size_t i=0; i< e.errors.size(); i++)
  		        ERROR_STREAM << e.errors[i].reason << " :: "
			                     << e.errors[i].desc  << " :: "
									         << e.errors[i].origin<< " :: "
									         << " code : " << e.errors[i].code 
									         << std::endl;
            this->com_status = "communication failed trying to reset" ;
            this->last_error = "Itest exception"
                             + std::string( e.errors[0].desc )
                             + std::string("]");
	        }
          catch (...)
          {
            com_state = COM_ERROR;
            ERROR_STREAM << "communication error [unknown exception caught]" << std::endl;
            this->com_status = "communication error trying to reset" ;
            this->last_error = "Unknown exception" ;
          }
          //- we can try to clear the COM_FATAL flag
          com_state = COM_OK;

  		  }
  		  break;


      //- UNHANDLED MSG --------------------
  		default:
  		  DEBUG_STREAM << "SteererTask::handle_message::unhandled msg type received" << std::endl;
  			break;
	  }

	  DEBUG_STREAM << "SteererTask::handle_message::message_handler:msg " 
                 << _msg.to_string() 
                 << " successfully handled" 
                 << std::endl;
  }

	// ============================================================================
	// SteererTask::get_data
	// ============================================================================
  void SteererTask::get_data (data & d)
  {
		DEBUG_STREAM << "SteererTask::get_data <-" << std::endl;
    { //- critical section
      yat::AutoMutex<> guard(this->m_lock);
      d = this->internal_data;
    }
    return;
  }

	// ============================================================================
	// SteererTask::reconnect_to_HW
	// ============================================================================
  void SteererTask::reconnect_to_HW (void)
  {
		DEBUG_STREAM << "SteererTask::reconnect_to_HW <-" << std::endl;

    ++this->communication_error_counter;

	  if(this->communication_error_counter > 3)
	  {
		  this->communication_error = true;
		  std::stringstream s; 
		  s << "steerer::reconnect_to_HW : error trying to reconnect to HW " 
			  << std::endl
			  << "tried 4 times"
			  << std::endl
			  << " [check network, BILT, try to restart device]"
			  << std::ends;
		  this->com_status = s.str();
      this->com_state = COM_FATAL;
		  return;
	  }

	  //- delete steerer class if necessary
	  if(this->m_steerer)
	  {
		  delete this->m_steerer;
		  this->m_steerer = 0;
	  }

	  try
	  {
		  this->m_steerer = new itest::TSteerer(steerer_config.ip_addr.c_str(), steerer_config.steerer_nb);
		  if(m_steerer == 0)
		  {
			  FATAL_STREAM << "ERROR : instanciation of TSteerer failed <m_steerer = 0>" << std::endl;
			  TangoSys_OMemStream oms;
			  oms << "steerer::reconnect_to_HW : instanciation of TSteerer failed"
					  << "[try to kill an restart device]" 
					  << std::ends;
			  ERROR_STREAM << oms.str() << std::endl;
			  this->com_status = oms.str();
        com_state = COM_FATAL;
        return;
		  }
	  }
    catch (Tango::DevFailed & df)
    {
		  FATAL_STREAM << "ERROR :instanciation of Steerer failed <catched DevFailed>" << std::endl;
  	  ERROR_STREAM << df << std::endl;
      Tango::DevError origin_of_pb = df.errors[df.errors.length() - 1];
      this->com_status = "steerer::reconnect_to_HW : device initialization failed ["
                         + std::string(origin_of_pb.desc.in())
                         + std::string("]");
      com_state = COM_FATAL;
      return;
    }
	  catch(itest::ItestException &e)
    {
		  FATAL_STREAM << "ERROR :instanciation of Steerer failed <catched ItestException!>" << std::endl;
		  for(size_t i=0; i< e.errors.size(); i++)
  		  ERROR_STREAM << e.errors[i].reason << " :: "
			               << e.errors[i].desc   << " :: "
									   << e.errors[i].origin << " :: "
									   << " code : " << e.errors[i].code 
									   << std::endl;
      this->com_status = std::string("device initialization failed [")
                         + e.errors[0].desc
                         + std::string("]");
      com_state = COM_FATAL;
      return;
    }
    catch (...)
    {
		  FATAL_STREAM << "ERROR :instanciation of Steerer failed <catched (...)>" << std::endl;
      ERROR_STREAM << "initialization failed [unknown exception caught]" << std::endl;
      this->com_status = "steerer::reconnect_to_HW : device initialization failed [try to kill and restart device]";
      com_state = COM_FATAL;
		  //- at this point an operator action is needed (try to restart device)
      return;
    }
	  //- successfull connection to HW
	  this->communication_error_counter = 0;
    com_state = COM_OK;
  }

} // namespace
