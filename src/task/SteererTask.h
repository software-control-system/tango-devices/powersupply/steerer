// ============================================================================
//
// = CONTEXT
//		Steerer Task implementation example
//
// = File
//		SteererTask.h
//
// = AUTHOR
//		jean coquet d'apres le code de Nicolas Leclercq - SOLEIL
//
// ============================================================================

#ifndef _STEERER_TASK_H_
#define _STEERER_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
//- #include <yat/memory/DataBuffer.h>
#include <yat4tango/DeviceTask.h>
#include <ClassSteerer.h>


namespace Steerer_ns
{
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kWRITE_CURRENT_MSG   (yat::FIRST_USER_MSG + 1000)
#define kLOCAL_CONTROL_MSG   (yat::FIRST_USER_MSG + 1001)
#define kLIBERA_CONTROL_MSG  (yat::FIRST_USER_MSG + 1002)
#define kCLEAR_ERROR_CNT_MSG (yat::FIRST_USER_MSG + 1003)
#define kRESET_MSG           (yat::FIRST_USER_MSG + 1004)
#define kCLEAR_ALARM_MSG     (yat::FIRST_USER_MSG + 1005)
#define kON_MSG              (yat::FIRST_USER_MSG + 1006)
#define kOFF_MSG             (yat::FIRST_USER_MSG + 1007)

// ============================================================================
// Utilities
// ============================================================================
//- the steerer configuration --------------
typedef struct steerer_cfg
{
public :
  std::string ip_addr;
  std::string axis_letter;
  int axis_number;
  short steerer_nb;
  //- Ctor -------------
  steerer_cfg ();
  //- Ctor -------------
  steerer_cfg ( const steerer_cfg & _src);
  //- Ctor -------------
  steerer_cfg (std::string _ip_addr, std::string _axis_letter, short _steerer_nb);
  //- operator = -------
  void operator = (const steerer_cfg & src);
} steerer_cfg;


typedef struct data
{
public :
  //- "switch" information
  bool is_fofb_controlled;
  //- "power state" information
  bool is_on;
  //- m_data structure
  double current;
  double voltage;
  std::string status;
  int state;
  //- "current" information
  double setpoint;
  //- f_data structure
  unsigned short nb_frames_sec;
  unsigned short nb_errors_sec;
  double sp_min;
  double sp_mean;
  double sp_max;
  unsigned short error_counter;

  //- Ctor -------------
  data ();
  //- Ctor -------------
  data ( const data & _src);
  //- operator = -------
  void operator = (const data & src);
} data;


  typedef enum
  {
    COM_NOT_INIT,
    COM_OK,
    COM_ERROR,
    COM_FATAL,
  } ComState;

  typedef enum
  {
    GET_M_DATA = 0,
    GET_F_DATA_1,
    GET_SETPOINT,
    GET_F_DATA_2,
    GET_SWITCH,
    GET_F_DATA_3,
    GET_POWER_STATE,
    GET_F_DATA_4,
    GET_MAX_INDEX
  } GetIndex;


// ============================================================================
// class: SteererTask
// ============================================================================

class SteererTask : public yat4tango::DeviceTask
{
public:

	//- ctor ------------------------------------
  SteererTask (size_t _periodic_timeout_ms, Tango::DeviceImpl * _host_device, steerer_cfg & cfg );

	//- dtor ------------------------------------
	virtual ~SteererTask (void);


  //- returns the last available data ---------
  void get_data (data & d);


  //- -----------------------------------------
  //- reconnection to HW ----------------------
  void reconnect_to_HW (void);

  //- get communication_state -----------------
  ComState get_com_state (void)
  {
    return com_state;
  }
  //- get communication status ----------------
  std::string get_com_status (void)
  {
    //- TODO : optimiser ca! 
    std::string s = " last error: " + last_error + "\n" + com_status;
    return s;
  }



protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg) throw (Tango::DevFailed);

private:

  //- utilities
  yat::Mutex m_lock;

   
  //- the configuration structure
  steerer_cfg steerer_config;

  //- the socket communication class
  itest::TSteerer * m_steerer;
	//- mdata structure contains axis data
	struct itest::mdata m_data;
	//- contains the state of the "switch" : 0 for local/device control 1 for Libera control

  //- fdata structure contains counting data informations ( librairie V203)
	struct itest::fdata f_data;

  //- internal stuffs
  bool communication_error;
  int communication_error_counter;
  std::string com_status;
  std::string last_error;
  ComState com_state;
  int get_index;
  //- internal data copy
  data internal_data;
  
  //- debug purposes
  yat::Timer dt;


};

} // namespace steerer_ns

#endif // _STEERER_TASK_H_
