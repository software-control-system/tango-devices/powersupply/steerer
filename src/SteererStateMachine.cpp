/*----- PROTECTED REGION ID(SteererStateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        SteererStateMachine.cpp
//
// description : State machine file for the Steerer class
//
// project :     Steerer
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <Steerer.h>

/*----- PROTECTED REGION END -----*/	//	Steerer::SteererStateMachine.cpp

//================================================================
//  States   |  Description
//================================================================
//  ON       |  power supply ON
//  OFF      |  power supply OFF
//  ALARM    |  power supply alarm : alarm condition, does not stop the PS
//  FAULT    |  power supply FAULT :
//           |  power supply is stopped (turned OFF)
//  DISABLE  |  no module BE548 configured in this BILT
//  UNKNOWN  |  other reasons, communication down...


namespace Steerer_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_isLiberaControlled_allowed()
 *	Description : Execution allowed for isLiberaControlled attribute
 */
//--------------------------------------------------------
bool Steerer::is_isLiberaControlled_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::isLiberaControlledStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::isLiberaControlledStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_isFofbControlled_allowed()
 *	Description : Execution allowed for isFofbControlled attribute
 */
//--------------------------------------------------------
bool Steerer::is_isFofbControlled_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::isFofbControlledStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::isFofbControlledStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_isOn_allowed()
 *	Description : Execution allowed for isOn attribute
 */
//--------------------------------------------------------
bool Steerer::is_isOn_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::isOnStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::isOnStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_current_allowed()
 *	Description : Execution allowed for current attribute
 */
//--------------------------------------------------------
bool Steerer::is_current_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Check access type.
	if ( type!=Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for WRITE 
		if (get_state()==Tango::OFF ||
			get_state()==Tango::FAULT ||
			get_state()==Tango::DISABLE ||
			get_state()==Tango::UNKNOWN)
		{
		/*----- PROTECTED REGION ID(Steerer::currentStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::currentStateAllowed_WRITE
			return false;
		}
		return true;
	}
	else

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT ||
			get_state()==Tango::UNKNOWN)
		{
		/*----- PROTECTED REGION ID(Steerer::currentStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	Steerer::currentStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_voltage_allowed()
 *	Description : Execution allowed for voltage attribute
 */
//--------------------------------------------------------
bool Steerer::is_voltage_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::voltageStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::voltageStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_setpoint_allowed()
 *	Description : Execution allowed for setpoint attribute
 */
//--------------------------------------------------------
bool Steerer::is_setpoint_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::setpointStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::setpointStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_spMin_allowed()
 *	Description : Execution allowed for spMin attribute
 */
//--------------------------------------------------------
bool Steerer::is_spMin_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::spMinStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::spMinStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_spMean_allowed()
 *	Description : Execution allowed for spMean attribute
 */
//--------------------------------------------------------
bool Steerer::is_spMean_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::spMeanStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::spMeanStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_spMax_allowed()
 *	Description : Execution allowed for spMax attribute
 */
//--------------------------------------------------------
bool Steerer::is_spMax_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::spMaxStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::spMaxStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_nbFrameSec_allowed()
 *	Description : Execution allowed for nbFrameSec attribute
 */
//--------------------------------------------------------
bool Steerer::is_nbFrameSec_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::nbFrameSecStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::nbFrameSecStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_nbErrorSec_allowed()
 *	Description : Execution allowed for nbErrorSec attribute
 */
//--------------------------------------------------------
bool Steerer::is_nbErrorSec_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::nbErrorSecStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::nbErrorSecStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_nbErrorCounter_allowed()
 *	Description : Execution allowed for nbErrorCounter attribute
 */
//--------------------------------------------------------
bool Steerer::is_nbErrorCounter_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::FAULT)
		{
		/*----- PROTECTED REGION ID(Steerer::nbErrorCounterStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::nbErrorCounterStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_On_allowed()
 *	Description : Execution allowed for On attribute
 */
//--------------------------------------------------------
bool Steerer::is_On_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for On command.
	/*----- PROTECTED REGION ID(Steerer::OnStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::OnStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_Off_allowed()
 *	Description : Execution allowed for Off attribute
 */
//--------------------------------------------------------
bool Steerer::is_Off_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Off command.
	/*----- PROTECTED REGION ID(Steerer::OffStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::OffStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_Reset_allowed()
 *	Description : Execution allowed for Reset attribute
 */
//--------------------------------------------------------
bool Steerer::is_Reset_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Reset command.
	/*----- PROTECTED REGION ID(Steerer::ResetStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::ResetStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_ClearAlarms_allowed()
 *	Description : Execution allowed for ClearAlarms attribute
 */
//--------------------------------------------------------
bool Steerer::is_ClearAlarms_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ClearAlarms command.
	/*----- PROTECTED REGION ID(Steerer::ClearAlarmsStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::ClearAlarmsStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_SetLiberaControl_allowed()
 *	Description : Execution allowed for SetLiberaControl attribute
 */
//--------------------------------------------------------
bool Steerer::is_SetLiberaControl_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetLiberaControl command.
	/*----- PROTECTED REGION ID(Steerer::SetLiberaControlStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::SetLiberaControlStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_SetLocalControl_allowed()
 *	Description : Execution allowed for SetLocalControl attribute
 */
//--------------------------------------------------------
bool Steerer::is_SetLocalControl_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetLocalControl command.
	/*----- PROTECTED REGION ID(Steerer::SetLocalControlStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::SetLocalControlStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_ClearErrorCounter_allowed()
 *	Description : Execution allowed for ClearErrorCounter attribute
 */
//--------------------------------------------------------
bool Steerer::is_ClearErrorCounter_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ClearErrorCounter command.
	/*----- PROTECTED REGION ID(Steerer::ClearErrorCounterStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::ClearErrorCounterStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : Steerer::is_SetFofbControl_allowed()
 *	Description : Execution allowed for SetFofbControl attribute
 */
//--------------------------------------------------------
bool Steerer::is_SetFofbControl_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetFofbControl command.
	/*----- PROTECTED REGION ID(Steerer::SetFofbControlStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	Steerer::SetFofbControlStateAllowed
	return true;
}


/*----- PROTECTED REGION ID(Steerer::SteererStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	Steerer::SteererStateAllowed.AdditionalMethods

}	//	End of namespace
