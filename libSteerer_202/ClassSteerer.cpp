// ============================================================================
//
// = CONTEXT
//    ITest Power Supplies - C++ Interface
//
// = FILENAME
//    ClassSteerer.cpp
//
// = AUTHOR
//    Sebastien Laurent - ITEST
//
// = VERSION
//	  2.02
//	
// ============================================================================
// modif V1.02
// modification des nom des méthodes(tout minuscule)
// modif V2.00
// lecture non bloquante (select) pour rupture de cable
// changement axe[] en axe()
// get_mdata et struct mdata
// modif V2.01
// correction bug constructeur de recopie (problème de segmentation)
// Ajout dans la classe de l'opérateur d'affectation =
// Passage du MaxTime à 10s
// Ajout de la commande get_bilt_vers
// modif V202
// modification récupération adresse IP steerer sous forme fonction addrIP_steerer()
// modification récupération numéro steerer num_steerer()
// ajout de la classe TBilt dans la pile des exceptions
// passage de l'héritage de public en private pour socket dans TBilt
// Initialisation des pointeurs à 0. 
// Pas d'exception dans le constructeur de socket, variable d'état dans les command/query de la classe qui déclenchent 
// des exceptions
// ============================================================================
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/time.h>
#include <fcntl.h>
#include <stdlib.h>

#include <netdb.h>

#include <iostream>
#include <sstream>

#include "ClassSteerer.h"
#include "Exception.h"

#include "TestClass.h"

namespace itest{
//---------------------------------------------------------------------------
//#define __ITEST_DEBUG
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------CLASSE SOCKET----------------------------------
//---------------------------------------------------------------------------
//SOCKET
//Création de la socket, connexion au BILT 
Socket::Socket(const char *StrIP):init_device_done(false)
{
	struct sockaddr_in adresse;
	int resultat,flag;
	struct timeval Tv;
	fd_set myset;

#ifdef __ITEST_DEBUG
std::cout<<"CREATE SOCKET"<<std::endl;
#endif

	Sockfd=0;
	Sockfd=socket(AF_INET, SOCK_STREAM, 0);								//création socket pour client
//test si Sock ok
	if(Sockfd == -1)
	{					//ItestException e(SOCK_ERROR_STR,strerror(errno),"itest::Socket::Socket",1,errno);	
						//throw e;
		return;			//modif V202 07/06/07
	}
//Set non blocking
	flag=fcntl(Sockfd, F_GETFL, 0);
	fcntl(Sockfd, F_SETFL, O_NONBLOCK|flag);

	adresse.sin_family=AF_INET;
	adresse.sin_addr.s_addr=inet_addr(StrIP);
	adresse.sin_port=htons(SOCK_PORT);					

	resultat=connect(Sockfd, (struct sockaddr *)&adresse, sizeof(adresse));	//connexion au serveur

	if(resultat==-1)
	{	
#ifdef __ITEST_DEBUG
std::cout<<"ERR_CONNECT"<<std::endl;
#endif
		if(errno==EINPROGRESS)
		{	
#ifdef __ITEST_DEBUG
std::cout<<"ERR_INPROGRESS"<<std::endl;
#endif
			Tv.tv_sec=MAXTIME_CONNECT_READ;						
			Tv.tv_usec=0;
			FD_ZERO(&myset);
			FD_SET(Sockfd,&myset);
			if(select(FD_SETSIZE ,NULL ,&myset, NULL, &Tv)<1)				//attend MAXTIME_CONNECT_READ
			{	close(Sockfd);
						//ItestException e(SOCK_ERROR_STR,"Could not connect with peripheral","itest::Socket::Socket");
						//throw e;
				return;	//modif V202 07/06/07
			}
#ifdef __ITEST_DEBUG
std::cout<<"END ERR_INPROGRESS"<<std::endl;
#endif
		}	
		else
		{	close(Sockfd);
						//ItestException e(SOCK_ERROR_STR,strerror(errno),"itest::Socket::Socket",1,errno);	
						//throw e;
			return;		//modif V202 07/06/07
		}
	}
	
	this->init_device_done=true;	//modif V202 07/06/07	

						//Set blocking
						//	fcntl(Sockfd, F_SETFL, flag);
#ifdef __ITEST_DEBUG
std::cout<<"END CREATED: "<<Sockfd<<std::endl;
#endif
}
//---------------------------------------------
Socket::~Socket(void)
{
#ifdef __ITEST_DEBUG
std::cout<<"DESTRUCT SOCKET num: "<<Sockfd<<std::endl;
#endif
	close(Sockfd);
	Sockfd=0;

#ifdef __ITEST_DEBUG
std::cout<<"DESTRUCT SOCKET"<<std::endl;
#endif
}
//---------------------------------------------
//SOCKET
//Envoie commande et reçoit 
//---------------------------------- 
void Socket::write(const std::string& Str)
{
	if(!this->init_device_done)			//modif V202 07/06/07
	{
		std::ostringstream Desc;
		Desc<<ITEST_EXCEPT_TRY_STR<<"\""<<Str<<std::ends;
		ItestException e("Object constructor failed",Desc.str().c_str(),"itest::Socket::write",1,errno);
		throw e;	
	}


	if(::write(Sockfd, Str.c_str(), strlen(Str.c_str()))==-1)
	{	std::ostringstream Desc;
		Desc<<ITEST_EXCEPT_TRY_STR<<"\""<<Str<<"\"->"<<strerror(errno)<<std::ends;
		ItestException e(CONNECT_FAILED_STR,Desc.str().c_str(),"itest::Socket::write",1,errno);
		throw e;}
}
//----------------------------------
//SOCKET
//Envoie commande et reçoit 
//---------------------------------- 
char *Socket::query(const std::string& Str)
{
	char *Ptr;	

	if(!this->init_device_done)			//modif V202 07/06/07		
	{
		std::ostringstream Desc;
		Desc<<ITEST_EXCEPT_TRY_STR<<"\""<<Str<<std::ends;
		ItestException e("Object constructor failed",Desc.str().c_str(),"itest::Socket::query",1,errno);
		throw e;	
	}

	R_Buff[0]=0;

	if(::write(Sockfd, Str.c_str(), strlen(Str.c_str()))==-1)
	{	
		std::ostringstream Desc;
		Desc<<ITEST_EXCEPT_TRY_STR<<"\""<<Str<<"\"->"<<strerror(errno)<<std::ends;
		ItestException e(CONNECT_FAILED_STR,Desc.str().c_str(),"itest::Socket::query",1,errno);
		throw e;}
//-----------------------------------------------------------------------------------------------
	int resultat,flag;
	struct timeval Tv;
	fd_set readfds;


	Tv.tv_sec=MAXTIME_CONNECT_READ;
	Tv.tv_usec=0;
	FD_ZERO(&readfds);
	FD_SET(Sockfd,&readfds);

	if((resultat=select(FD_SETSIZE ,&readfds ,NULL, NULL, &Tv))<1)
	{	//close(Sockfd);
#ifdef __ITEST_DEBUG
std::cout<<"ERREUR: "<<strerror(errno)<<"\nRES: "<<resultat<<std::endl;
#endif
		ItestException e(SOCK_ERROR_STR,"Could not read data on peripheral","itest::Socket::query");
		throw e;}

	if(::read(Sockfd, &R_Buff[0], BUFFSIZE)==-1)
	{		
#ifdef __ITEST_DEBUG
std::cout<<"ERREUR: "<<strerror(errno)<<"\nRES: "<<resultat<<std::endl;
#endif
		std::ostringstream Desc;
		Desc<<ITEST_EXCEPT_TRY_STR<<"\""<<Str<<"\"->"<<strerror(errno)<<std::ends;
		ItestException e(CONNECT_FAILED_STR,Desc.str().c_str(),"itest::Socket::query",1,errno);
		throw e;}
//-----------------------------------------------------------------------------------------------
	if((R_Buff[0]==0)||(R_Buff[0]=='\n'))	//rien reçu modif V101
	{	std::ostringstream Desc;
		Desc<<ITEST_EXCEPT_TRY_STR<<"\""<<Str<<"\""<<std::ends;
		ItestException e(CONNECT_FAILED_STR,Desc.str().c_str(),"itest::Socket::query",1,errno);
		throw e;}

	if((Ptr=strchr(&R_Buff[0],'\n'))!=NULL)		//modif V101
		*Ptr=0;

	return &R_Buff[0];
}
//--------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------CLASSE BILTRW----------------------------------
//---------------------------------------------------------------------------
TBilt::TBilt(const char *StrIP):Socket(StrIP)
{
#ifdef __ITEST_DEBUG
std::cout<<"BILT CREATE"<<std::endl;
#endif
}
//----------------------------------
TBilt::~TBilt(void)
{
#ifdef __ITEST_DEBUG
std::cout<<"BILT DESTRUCT"<<std::endl;
#endif
}
//----------------------------------
void TBilt::write(const std::string& _Str) throw (itest::ItestException)
{
	std::ostringstream oss;
	char *Ptr;

	oss<<_Str<<std::endl; 			//modif V101 syst:err? à part

	Socket::write(oss.str());
	oss.str("");
	oss<<Q_SYSTERR<<std::endl;

	try{									//ajout try/catch modif V202 05/06/07
		Ptr=Socket::query(oss.str());
	}
	catch(itest::ItestException &e)
	{	
		e.push_error("Bilt write failed","Could not read data on peripheral","itest::TBilt::write");
		throw e;	
	}

	if((this->NumLastErr=atoi(Ptr))!=0)
	{
		std::ostringstream Desc;

		this->StrLastErr=strchr(Ptr,',');
		strcpy(&BuffLastErr[0],++this->StrLastErr);

		this->StrLastErr=&BuffLastErr[0];

		Desc<<ITEST_EXCEPT_TRY_STR<<"\""<<_Str<<"\"->"<<this->StrLastErr<<std::ends;
		
		ItestException e("Bilt command failed",Desc.str().c_str(),"itest::TBilt::write",1,this->NumLastErr);
		throw e;
	}
}
//----------------------------------
char *TBilt::query(const std::string& _Str) throw (itest::ItestException)
{
	std::ostringstream oss;

	oss<<_Str<<std::endl;

	try{									//ajout try/catch modif V202 05/06/07
		return Socket::query(oss.str());
	}
	catch(itest::ItestException &e)
	{	
		e.push_error("Bilt query failed","Could not read data on peripheral","itest::TBilt::query");
		throw e;	
	}
}

//--------------------------------------------------------------------------
//---------------------------------------------------------------------------
//-------------------------CLASSE STEERER----------------------------------
//---------------------------------------------------------------------------
TSteerer::TSteerer(const char *_AddrIp,int NumSteer) throw (itest::ItestException) :Bilt(0)
{
try{
#ifdef __ITEST_DEBUG
std::cout<<"CREATE TSTEERER"<<std::endl;
#endif


	NumSteerer=NumSteer;
	AddrIP=&StrAddrIp[0];
	strcpy((char *)AddrIP,_AddrIp); 

	Axis[0]=0;										//modif V202 05/06/07
	Axis[1]=0;										//modif V202 05/06/07

	Bilt=new TBilt(AddrIP);

#ifdef __ITEST_DEBUG
std::cout<<"Bilt=new"<<std::endl;
#endif

	Axis[0]=new TAxis(Bilt	,NumSteerer ,X);
	Axis[1]=new TAxis(Bilt	,NumSteerer ,Y);

#ifdef __ITEST_DEBUG
std::cout<<"CREAT STEER: "<<AddrIP<<std::endl;
std::cout<<"NumSteerer: "<<NumSteerer<<std::endl;;
#endif
}
		/*catch(itest::ItestException &e)
		{	
			std::ostringstream Desc;

			Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;
		
			e.push_error("Steerer could not be created",	Desc.str().c_str()	,"itest::TSteerer::TSteerer");
			throw e;	
		}*/
catch(...)
{
	throw ItestException("Unknown exception","Unknown exception caugth","itest::TSteerer::TSteerer"); 
}
}
//----------------------------------
TSteerer::TSteerer(const TSteerer & Steer) throw (itest::ItestException) : Bilt(0)
{
try{
	NumSteerer=Steer.NumSteerer;
	AddrIP=&StrAddrIp[0];							//modif V201 07/03/07
	strcpy((char *)AddrIP,&Steer.StrAddrIp[0]); 	//modif V201 07/03/07

	Axis[0]=0;										//modif V202 05/06/07
	Axis[1]=0;										//modif V202 05/06/07

	Bilt=new TBilt(AddrIP);	//TBilt *Bilt(AddrIP); modif V201 07/03/07

	Axis[0]=new TAxis(Steer.Bilt	,Steer.NumSteerer ,X);
	Axis[1]=new TAxis(Steer.Bilt	,Steer.NumSteerer ,Y);

#ifdef __ITEST_DEBUG
std::cout<<"COPY STEER"<<std::endl;
#endif
}
catch(itest::ItestException &e)
{	
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;
	e.push_error("Steerer could not be copied",Desc.str(), "itest::TSteerer::TSteerer");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TSteerer::TSteerer");
	throw e; 
}
}
//----------------------------------
TSteerer & TSteerer::operator = (const TSteerer & Steer) throw (itest::ItestException) //modif V201 07/03/07
{
try{

	//- no self assignment
  	if (this == &Steer)
    	return *this;

#ifdef __ITEST_DEBUG
std::cout<<"OPERATOR = STEER"<<std::endl;
#endif

	delete Axis[0],Axis[1];
	delete Bilt;

	NumSteerer=Steer.NumSteerer;
	AddrIP=&StrAddrIp[0];							
	strcpy((char *)AddrIP,&Steer.StrAddrIp[0]); 	

	Bilt=0;											//modif V202 05/06/07
	Axis[0]=0;										//modif V202 05/06/07
	Axis[1]=0;										//modif V202 05/06/07

	Bilt=new TBilt(AddrIP);	

	Axis[0]=new TAxis(Steer.Bilt	,Steer.NumSteerer ,X);
	Axis[1]=new TAxis(Steer.Bilt	,Steer.NumSteerer ,Y);

#ifdef __ITEST_DEBUG
std::cout<<"OPERATOR = STEER"<<std::endl;
#endif

	  return *this;
}
catch(itest::ItestException &e)
{	
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;
	e.push_error("Steerer could not be affected",Desc.str(), "itest::TSteerer::operator=");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TSteerer::operator=");
	throw e; 
}
}
//----------------------------------
TSteerer::~TSteerer(void)
{
	delete Axis[0],Axis[1];
	delete Bilt;

#ifdef __ITEST_DEBUG
std::cout<<"DESCTRUCT STEER"<<std::endl;
#endif
}
//----------------------------------
TAxis & TSteerer::axis(int Num)	//ajout const modif V202 05/06/07
{
	if(Num<0 || Num>1)
	{
		std::ostringstream Desc;
		Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;

		ItestException e("Axis number does not exist",Desc.str(),"itest::TSteerer::axis");
		throw e;}

	return *this->Axis[Num];
}
//---------------------------------------------------------------------------
//GROUPE 
//Clear toutes les alarmes du groupe
//Efface tous les défauts de l'axe du Steerer
//---------------------------------- 
void TSteerer::clear_alarm(void) throw (itest::ItestException)
{
try{
	this->Axis[X]->clear_alarm();
	this->Axis[Y]->clear_alarm();
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;
	e.push_error("Alarm could not be cleared",Desc.str(), "itest::TSteerer::clear_alarm");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TSteerer::clear_alarm");
	throw e; 
}
}
//---------------------------------------------------------------------------
void TSteerer::set_state(bool val) throw (itest::ItestException)
{
try{
	this->Axis[X]->set_state(val);
	this->Axis[Y]->set_state(val);
}
catch(itest::ItestException &e)
{	
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;
	e.push_error("State could not be changed",Desc.str(), "itest::TSteerer::set_state");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TSteerer::set_state");
	throw e; 
}
}
//---------------------------------------------------------------------------
void TSteerer::set_switch(bool val) throw (itest::ItestException)
{
try{
	this->Axis[X]->set_switch(val);
	this->Axis[Y]->set_switch(val);
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<std::ends;
	e.push_error("Switch could not be changed",Desc.str(), "itest::TSteerer::set_switch");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TSteerer::set_switch");
	throw e; 
}
}
//---------------------------------------------------------------------------
void TSteerer::clear_all_err(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;
	int NumBilt_Err;

	oss<<Q_SYSTERR;  	//modif v2.00 oss<<Q_SYSTERR<<std::endl;

	do
	{
		NumBilt_Err=atoi(Bilt->query(oss.str()));	
#ifdef __ITEST_DEBUG
std::cout<<"BOUCLE ERR"<<std::endl;
#endif
	}
	while(NumBilt_Err!=0);
}
catch(itest::ItestException &e)
{	
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;
	e.push_error("Alarm could not be cleared",Desc.str(), "itest::TSteerer::clear_all_err");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TSteerer::clear_all_err");
	throw e; 
}
}
//---------------------------------------------------------------------------
const char * TSteerer::get_bilt_vers(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;
	char *Ptr;

	oss<<SYSTVERS<<std::endl; 

	Ptr=Bilt->query(oss.str());

	return Ptr;
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;
	e.push_error("Bilt firware version could not be read",Desc.str(), "itest::TSteerer::get_bilt_vers");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TSteerer::get_bilt_vers");
	throw e; 
}
}
//---------------------------------------------------------------------------
//-----------------------------CLASSE AXE------------------------------------
//---------------------------------------------------------------------------
//Constructeur de l'axe
TAxis::TAxis(TBilt *_Bilt, int _NumSteerer, bool _Axis)
{
	NumSteerer=_NumSteerer;
	Axis=_Axis;
	Bilt=_Bilt;

	Group=((NumSteerer&1)?P1:P3)+Axis;									//groupe func de Num Steerer et Axe 
	Inst=((NumSteerer&1)?INST_STEER1_AXE_X:INST_STEER2_AXE_X)+Axis*2;	//instrument  func de Num Steerer et Axe 
}
//---------------------------------------------------------------------------
//GROUPE 
//Récupère l'état du groupe
//1=OFF;2=ON;3=WARNING;4=ALARM;5=STOP
//---------------------------------- 
int TAxis::get_state(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;

	oss<<":p"<<this->Group<<Q_STAT; 

	return (atoi(Bilt->query(oss.str()))-1);
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<"/P"<<this->Group<<")"<<std::ends;
	e.push_error("State could not be read",Desc.str(), "itest::TAxis::get_state");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::get_state");
	throw e; 
}
}
//---------------------------------------------------------------------------
//GROUPE 
//Allume ou éteind le groupe
//Allume ou éteind l'axe du Steerer
//0=OFF;1=ON
//---------------------------------- 
void TAxis::set_state(bool val) throw (itest::ItestException)
{ 
try{
	std::ostringstream oss;

	oss<<":i"<<this->Inst<<";:p"<<this->Group<<STAT<<val+1; //oss<<":p"<<this->Group<<STAT<<val+1; modif V101 

	Bilt->write(oss.str());
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<"/P"<<this->Group<<")"<<std::ends;
	e.push_error("State could not be changed",Desc.str(), "itest::TAxis::set_state");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::set_state");
	throw e; 
}
}
//---------------------------------------------------------------------------
//GROUPE 
//Clear toutes les alarmes du groupe
//Efface tous les défauts de l'axe du Steerer
//---------------------------------- 
void TAxis::clear_alarm(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;

	oss<<":i"<<this->Inst<<";:p"<<this->Group<<STATCLEAR;//oss<<":p"<<this->Group<<STATCLEAR;modif V101 

	Bilt->write(oss.str());
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<"/P"<<this->Group<<")"<<std::ends;
	e.push_error("Alarm could not be cleared",Desc.str(), "itest::TAxis::clear_alarm");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::clear_alarm");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Lecture de la mesure en courant 
//Lecture de la mesure en courant de l'axe
//---------------------------------- 
double TAxis::get_measure_current(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;
	char *Ptr;

	oss<<":i"<<this->Inst<<Q_MCURR; 

	Ptr=Bilt->query(oss.str());

	return atof(Ptr);
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Current measure could not be read",Desc.str(), "itest::TAxis::get_measure_current");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::get_measure_current");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Lecture de la mesure en tension 
//Lecture de la mesure en tension de l'axe
//---------------------------------- 
double TAxis::get_measure_voltage(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;
	char *Ptr;

	oss<<":i"<<this->Inst<<Q_MVOLT; 

	Ptr=Bilt->query(oss.str());

	return atof(Ptr);
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Voltage measure could not be read",Desc.str(), "itest::TAxis::get_measure_voltage");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::get_measure_voltage");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Lecture de mdata : NoModule,State,Fail,MeasU,MeasI
//State : 0=OFF,1=ON,2=WARNING,3=ALARM
//Lecture de l'état et des paramètres de l'alimentation de l'axe 
//---------------------------------- 
void TAxis::get_mdata(struct mdata *mydata) throw (itest::ItestException)
{
try{

	std::ostringstream oss;
	char *Ptr;

	oss<<":i"<<this->Inst<<";:p"<<this->Group<<Q_MDATA; 

	Ptr=Bilt->query(oss.str());
#ifdef __ITEST_DEBUG
std::cout<<"GET_MADATA: "<<Ptr<<std::endl;
#endif
	std::string data(Ptr);
	//------------
	int i=0,Stat=0;
	int start,end;

	enum {STAT1=2,STAT2};
	enum {FAIL1=7,FAIL2_MV1,MV2_MC1,MC2};


	while((i=data.find_first_of(",",i)) != std::string::npos)
	{Stat++;
	
		switch(Stat)
		{
			case STAT1	:	start=i+1;
							break;
			case STAT2	:	end=i;
							mydata->state=atoi(data.substr(start,end-start).c_str())-1;	//STATE GROUP
							break;
			case FAIL1	:	start=i+1;
							break;
			case FAIL2_MV1:	end=i;
							mydata->fail=data.substr(start,end-start);					//FAIL
							start=i+1;
							break;
			case MV2_MC1:	end=i;
							mydata->measvolt=atof(data.substr(start,end-start).c_str());	//Meas volt
							start=i+1;
							break;
			default		:	break;
		}
	i++;} 

	end=data.length();
	mydata->meascurr=atof(data.substr(start,end-start).c_str());						//Meas curr
	//----------
	mydata->steerer=this->NumSteerer;
	mydata->axis=this->Axis;
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Mdata could not be read",Desc.str(), "itest::TAxis::get_mdata");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::get_mdata");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Lecture de l'état du switch RS485/TCP
//Lecture de l'état du switch liaison rapide / (liaison locale/ethernet)
//---------------------------------- 
int TAxis::get_switch(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;
	char *Ptr;

	oss<<":i"<<this->Inst<<Q_SWITCH; 

	Ptr=Bilt->query(oss.str());

	return atoi(Ptr);
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Switch could not be read",Desc.str(), "itest::TAxis::get_switch");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::get_switch");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Consigne en courant sur instrument
//Lecture de la consigne en courant initiale sur l'axe du steerer
//valeur flottante
//---------------------------------- 
double TAxis::get_current(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;
	char *Ptr;

	oss<<":i"<<this->Inst<<Q_CURR; 

	Ptr=Bilt->query(oss.str());

	return atof(Ptr);
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Current value could not be read",Desc.str(), "itest::TAxis::get_current");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::get_current");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Alarme sur instrument
//Lecture de l'alarme courante sur l'axe du steerer
//valeur int
//---------------------------------- 
int TAxis::get_num_alarm(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;
	char *Ptr;

	oss<<":i"<<this->Inst<<Q_FAIL; 

	Ptr=Bilt->query(oss.str());

	return atoi(Ptr);
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Alarm number could not be read",Desc.str(), "itest::TAxis::get_num_alarm");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::get_num_alarm");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Alarme sur instrument
//Lecture de l'alarme courante sur l'axe du steerer
//valeur int
//---------------------------------- 
const char * TAxis::get_str_alarm(void) throw (itest::ItestException)
{
try{
	std::ostringstream oss;
	char *Ptr;

	oss<<":i"<<this->Inst<<Q_FAILSTR; 

	Ptr=Bilt->query(oss.str());

	return Ptr;
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Alarm string could not be read",Desc.str(), "itest::TAxis::get_str_alarm");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::get_str_alarm");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Consigne en courant sur instrument
//programme une consigne en courant initiale sur l'axe du steerer
//valeur flottante
//---------------------------------- 
//ne convient pas car si déclenche exception socket
//ne passe pas par déclenchement itest::Set_Current...
void TAxis::set_current(double ValF) throw (itest::ItestException)
{
try{
	std::ostringstream oss;

	oss<<":i"<<this->Inst<<CURR<<ValF; 

	Bilt->write(oss.str());
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Current could not be changed",Desc.str(), "itest::TAxis::set_current");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::set_current");
	throw e; 
}
}
//---------------------------------------------------------------------------
//INSTRUMENT BE548
//Change l'état du switch RS485/TCP
//Change l'état du switch liaison rapide / (liaison locale/ethernet)
//---------------------------------- 
void TAxis::set_switch(bool val) throw (itest::ItestException)
{
try{
	std::ostringstream oss;

	oss<<":i"<<this->Inst<<SWITCH<<val; 

	Bilt->write(oss.str());
}
catch(ItestException &e)
{
	std::ostringstream Desc;
	Desc<<ITEST_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;
	e.push_error("Switch state could not be changed",Desc.str(), "itest::TAxis::set_switch");
	throw e;
}
catch(...)
{
	std::ostringstream Desc;
	Desc<<UNKNOW_EXCEPT_STEER_STR<<this->NumSteerer<<"/Axis "<<(this->Axis?"Y":"X")<<"(I"<<this->Inst<<")"<<std::ends;

	ItestException e("Unknown error",Desc.str(),"itest::TAxis::set_switch");
	throw e; 
}
}
//---------------------------------------------------------------------------
}	//end namespace
//---------------------------------------------------------------------------

