// ============================================================================
//
// = CONTEXT
//    ITest Power Supplies - C++ Interface
//
// = FILENAME
//    ClassSteerer.h
//
// = AUTHOR
//    Sebastien Laurent - ITEST
//
// = VERSION
//	 2.02
//
// ============================================================================
#ifndef _ITEST_CLASSSTEERER_H_
#define _ITEST_CLASSSTEERER_H_
//---------------------------------------------------------------------------
#include <string>
#include "Exception.h"

namespace itest{

#define INST_STEER1_AXE_X	2	//Instrument de l'axe X du steerer impaire
#define INST_STEER2_AXE_X	8	//Instrument de l'axe X du steerer paire

#define P1	1					//groupe de BILT
#define P2	2
#define P3	3
#define P4	4

#define BUFFSIZE	130			//taille buffer de caractère de réception
#define SOCK_PORT	5025		//Port de communication socket

#define MAXTIME_CONNECT_READ	10	//temps max d'attente pour une connexion et une lecture sur BILT modif V201 

#define X	0
#define Y	1

#define CONF	0
#define LIBERA	1

#define STEERER_OFF		0
#define STEERER_ON		1

#define Q_SYSTERR	":syst:err?"

#define Q_MCURR	";:meas:curr?"
#define Q_MVOLT	";:meas:volt?"
#define Q_IDATA ";:idata?"
#define Q_MDATA ";:mdata?"
#define Q_SWITCH ";:switch?"
#define Q_CURR	";:curr?"
#define Q_FAIL ";:lim:fail?"
#define Q_FAILSTR ";:lim:fail:str?"

#define Q_STAT ":stat?"

#define SWITCH  ";:switch "
#define CURR	";:curr "
#define STAT	":stat "
#define STATCLEAR ":stat:clear"

#define SYSTVERS	":syst:vers?"

//---------------------------------------------------------------------------
#define ITEST_EXCEPT_TRY_STR "ItestException exception caught while trying "
#define ITEST_EXCEPT_STEER_STR "ItestException exception caught on steerer"

#define UNKNOW_EXCEPT_STEER_STR "Unknown exception caugth on steerer"

#define SOCK_ERROR_STR "Socket error"
#define CONNECT_FAILED_STR "Connection failed"
//---------------------------------------------------------------------------
class Socket{
	private:	
		int Sockfd;
		char R_Buff[BUFFSIZE];	
		bool init_device_done;		//modif V202 07/06/07 pas d'exception dans constructeur mais dans fonctions membres

	protected:
	public:	
		Socket(const char *StrIP);
		~Socket(void);
		char *query(const std::string& Str);	
		void write(const std::string& Str);
};
//----------------------------------
class TBilt : private Socket	//public Socket modif V202 05/06/07
{
	private:	
		char BuffLastErr[50];
		int NumLastErr;
		const char *StrLastErr;				//modif V202 07/06/07 passage en private
	protected:
	public:	
		TBilt(const char *StrIP);
		~TBilt(void);
		void write(const std::string& _Str) throw (itest::ItestException);
		char *query(const std::string& _Str) throw (itest::ItestException);	//ajout de const modif V202 05/06/07
};
//----------------------------------
class TAxis{

	private:
		int Group;
		int Inst;
		int *PtrSock;
		char *Ptr_R_Buff;
		int NumSteerer;
		bool Axis;
		TBilt *Bilt;
	public:
		TAxis(TBilt *_Bilt, int _NumSteerer, bool _Axis);

		double get_measure_current(void) throw (itest::ItestException);
		double get_measure_voltage(void) throw (itest::ItestException);
		void get_mdata(struct mdata *) throw (itest::ItestException);		
		int get_switch(void) throw (itest::ItestException);
		double get_current(void) throw (itest::ItestException);
		int get_state(void) throw (itest::ItestException);
		int get_num_alarm(void) throw (itest::ItestException);
		const char * get_str_alarm(void) throw (itest::ItestException);

		void set_state(bool val) throw (itest::ItestException);
		void set_current(double ValF) throw (itest::ItestException);
		void set_switch(bool val) throw (itest::ItestException);
		void clear_alarm(void) throw (itest::ItestException);
};
//----------------------------------
class TSteerer
{
	protected:
	private:
		char StrAddrIp[20];
		TBilt *Bilt;
		TAxis *Axis[2];

		int NumSteerer;			//passe en private modif V202 05/06/07
		const char *AddrIP;		//passe en private modif V202 05/06/07		

	public:

		TSteerer(const char *_AddrIp,int NumSteer) throw (itest::ItestException);
		~TSteerer(void);
		TSteerer(const TSteerer & Steer) throw (itest::ItestException);
		TSteerer & operator=(const TSteerer & Steer) throw (itest::ItestException);	//modif V201 07/03/07

		int num_steerer(void){return NumSteerer;}			//modif V202 05/06/07
		const char *addrIP_steerer(void){return AddrIP;}		//modif V202 05/06/07

		TAxis & axis(int Num);

		void clear_all_err(void) throw (itest::ItestException);
		void clear_alarm(void) throw (itest::ItestException);
		void set_switch(bool val) throw (itest::ItestException);
		void set_state(bool val) throw (itest::ItestException);

		const char * get_bilt_vers(void) throw (itest::ItestException);

};
//---------------------------------------------------------------------------
//Structure de données de Mdata
struct mdata{
	int steerer;
	int axis;
	int state;
	std::string fail;
	double measvolt;
	double meascurr;
};
//---------------------------------------------------------------------------
}	//end namespace
//---------------------------------------------------------------------------
#endif //_ITEST_CLASSSTEERER_H_
