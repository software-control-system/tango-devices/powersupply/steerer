#include <sstream>
#include <iostream>

#include <vector>

#include "ClassSteerer.h"
#include "Exception.h"
//g++ -o TestClass TestClass.cpp -L./ -lSteer

using namespace itest;

TSteerer *Steer;

void *func1_thread(void *arg);

main (int argc, char **argv)
{ 
	int val=0;
	struct mdata myDataX_Steer1;
	struct fdata myFDataX_Steer1;

	try{

		Steer=0;
		Steer=new TSteerer("192.168.150.215",1);
		std::cout<<Steer->addrIP_steerer()<<std::endl;
		Steer->axis(1).set_current(1);
		Steer->axis(0).get_mdata(&myDataX_Steer1);

		std::cout<<myDataX_Steer1.fail<<","<<myDataX_Steer1.measvolt<<"/"<<myDataX_Steer1.meascurr<<std::endl;

	}
	catch(ItestException &e)
	{
		for(int i=0;i<e.errors.size();i++)		//gestion des exceptions empilées dans le vecteur d'erreur
       	{
			std::cout<<e.errors[i].reason<<" , "<<e.errors[i].desc<<" , "<<e.errors[i].origin<<" , "<<
					e.errors[i].code<<std::endl;
		}
		delete Steer;
	}	

	try{
		Steer=new TSteerer("192.168.150.215",1);

		Steer->axis(1).clear_ferrors();
		Steer->axis(1).set_state(1);
		Steer->axis(1).set_switch(1);

		for(int i=0;i<10;i++)
		{
			std::cout<<Steer->addrIP_steerer()<<std::endl;

			std::cout<<Steer->num_steerer()<<std::endl;

			Steer->axis(1).get_mdata(&myDataX_Steer1);
			sleep(1);
			Steer->axis(1).set_current(1);

			Steer->axis(1).get_fdata(&myFDataX_Steer1);

			std::cout<<"-----------------"<<
					"nbr frame:"<<myFDataX_Steer1.nbrframe_sec<<std::endl<<
					"nbr errsec:"<<myFDataX_Steer1.nbrferr_sec<<std::endl<<
					"min:"<<myFDataX_Steer1.min_sett_sec<<std::endl<<
					"max:"<<myFDataX_Steer1.max_sett_sec<<std::endl<<
					"average:"<<myFDataX_Steer1.average_sett_sec<<std::endl<<
					"nbr err:"<<myFDataX_Steer1.nbrferr<<std::endl<<
				"-----------------"<<std::endl;
		}
		Steer->axis(1).set_state(0);	
		delete Steer;
	}
	catch(ItestException &e)
	{
		for(int i=0;i<e.errors.size();i++)		//gestion des exceptions empilées dans le vecteur d'erreur
       	{
			std::cout<<e.errors[i].reason<<" , "<<e.errors[i].desc<<" , "<<e.errors[i].origin<<" , "<<
					e.errors[i].code<<std::endl;
		}
		delete Steer;
	}	
/*
	try{

		TSteerer Steerer3("192.168.150.127",2);
		TSteerer Steerer4("192.168.150.127",2);
		TSteerer Steerer1("192.168.150.127",1);
		TSteerer Steerer2("192.168.150.127",1);


		std::cout<<"VERSION: "<<Steerer1.get_bilt_vers()<<std::endl;	//version du firware de BILT

		struct mdata myDataX_Steer1,myDataY_Steer1;

		Steerer1.clear_all_err();	
        Steerer1.axis(X).set_current(1.2);		//Envoie de la consigne en courant à l'axe X du steerer
        Steerer1.axis(X).get_mdata(&myDataX_Steer1);
        if(myDataX_Steer1.state==3)
			{ Steerer1.axis(X).clear_alarm();  Steerer1.axis(X).get_mdata(&myDataX_Steer1); }
        if(myDataX_Steer1.state==0)
			{ Steerer1.axis(X).set_state(STEERER_ON); }
 
		sleep(1);

		Steerer1.axis(X).set_switch(LIBERA);

        Steerer1.axis(X).get_mdata(&myDataX_Steer1);
        Steerer1.axis(Y).get_mdata(&myDataY_Steer1);	     


		Steerer1.axis(X).set_state(OFF);

		std::cout<<"Meas volt: "<<myDataX_Steer1.measvolt<<std::endl;

   }catch(ItestException &e)			
   {for(int i=0;i<e.errors.size();i++)		//gestion des exceptions empilées dans le vecteur d'erreur
         std::cout<<e.errors[i].reason<<" , "<<e.errors[i].desc<<" , "<<e.errors[i].origin<<" , "<<e.errors[i].code<<std::endl;}
*/

//std::cout<<"NBR Err: "<<val++<<std::endl;
	
//};		
    return 0;
}
